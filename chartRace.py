import pandas as pd
import numpy as np
import csv
import collections

def makeChartRace(index,name,filename = 'none.csv'):
	newIndex = ["0"]
	for x in range(len(index)):
		if(newIndex[-1] != index[x]):
			newIndex.append(index[x])
	newIndex = newIndex[1:]

	newNames = ["0"]
	for x in range(len(name)):
		if(haveInList(newNames,name[x])):
			newNames.append(name[x])
	newNames = newNames[1:]

	df = pd.DataFrame(np.zeros((len(newNames),len(newIndex))),index=newNames, columns=newIndex)
	
	countByName=np.zeros(len(newNames))
	lastDay = index[0]
	for x in range(len(index)):
		if(lastDay != index[x]):
			#Procura o index do  dia
			for coluna in range(len(newIndex)):
				if(newIndex[coluna]==lastDay): break
			for linha in range(len(newNames)):
				df.iloc[linha][coluna] = countByName[linha]
				#df.loc[linha, coluna] = countByName[linha]
			lastDay = index[x]
		#Incrementa o nome
		for linha in range(len(newNames)):
			if(newNames[linha]==name[x]):
				countByName[linha] += 1
				break
	#preenche o ultimo dia
	for linha in range(len(newNames)):
		df.iloc[linha][-1] = countByName[linha]

	df.to_csv('output/chartRace '+filename, sep=',', encoding='utf-8')


def haveInList(list,name):
	for x in range(len(list)):
		if(list[x]==name): return False
	return True

def makeCorrelation(lista,filename = 'none.csv'):
	newList = []
	for x in range(len(lista[0])):
		newList.append(lista[0][x]+','+lista[1][x])

	RankList=collections.Counter(newList)
	y = list(RankList.values())
	t = list(RankList.keys())
	
	from_=[]
	to_=[]
	for x in range(len(t)):
		a,b =t[x].split(',')
		from_.append(a)
		to_.append(b)

	correlation = {'source': from_, 'target': to_,'value':y}
	df = pd.DataFrame(correlation)
	print (df)
	df.to_csv('output/Correlation '+filename, sep=',', encoding='utf-8')

	#tabela 2
	todosNomes = from_
	for x in range(len(to_)):
		todosNomes.append(to_[x])
		
	todosNomes=collections.Counter(todosNomes)
	todosNomesSZ = list(todosNomes.values())
	todosNomesID = list(todosNomes.keys())
	
	correlation = {'ID': todosNomesID, 'size': todosNomesSZ}
	df = pd.DataFrame(correlation)
	print (df)
	df.to_csv('output/Correlation2 '+filename, sep=',', encoding='utf-8')
