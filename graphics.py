# Libraries
import matplotlib.pyplot as plt
import pandas as pd
from math import pi
import seaborn as sns
import numpy as np
import networkx as nx
from wordcloud import WordCloud
import numpy as np
from PIL import Image

def makeRadar(listName,listKeys,titulo = 'none',legenda='none'):
	plt.figure()
	LScale = max(listKeys)
	for x in range(10):
		if(LScale<10**x): break
	divi=int((10**x)/10)
	LScale=LScale-(LScale%divi)+divi
	listScale = [int(LScale*(1/5)),int(LScale*(2/5)),int(LScale*(3/5)),int(LScale*(4/5)),LScale]
	SRTlistScale = [str(listScale[0]),str(listScale[1]),str(listScale[2]),str(listScale[3]),str(listScale[4])]
	scaleAngle = 360/len(listName)
	scaleAngle /= 2

	df = pd.DataFrame({'group': ['values']})
	for x in range (len(listName)): 
		df[listName[x]] = pd.Series(listKeys[x])

	# number of variable
	categories=list(df)[1:]
	N = len(categories)
	
	# We are going to plot the first line of the data frame.
	# But we need to repeat the first value to close the circular graph:
	values=df.loc[0].drop('group').values.flatten().tolist()
	values += values[:1]
	values
	 
	# What will be the angle of each axis in the plot? (we divide the plot / number of variable)
	angles = [n / float(N) * 2 * pi for n in range(N)]
	angles += angles[:1]
	 
	# Initialise the spider plot
	ax = plt.subplot(111, polar=True)
	 
	# Draw one axe per variable + add labels labels yet
	plt.xticks(angles[:-1], categories, color='grey', size=8)
	 
	# Draw ylabels
	ax.set_rlabel_position(scaleAngle)#em graus
	plt.yticks(listScale, SRTlistScale, color="grey", size=7)
	plt.ylim(0,LScale)
	 
	# Plot data
	ax.plot(angles, values, linewidth=1, linestyle='solid',label=legenda)
	 
	# Fill area
	ax.fill(angles, values, 'b', alpha=0.1)

	#Informações extra
	plt.title(titulo)
	plt.legend(loc='lower right', bbox_to_anchor=(1.2, -0.1))
	plt.savefig('output/'+titulo+'.png')


def changeStart(listValue,start = 1):
	newList=[]
	for x in range(len(listValue)):
		newList.append(listValue[start-len(listValue)+x])
	return newList

def getMinPos(listValue):
	value = listValue[0]
	x=0
	for x in range(len(listValue)):
		if(value>listValue[x]):
			value=listValue[x]
			pos = x
	return pos

def makeArea(x,y,titulo = 'none',legenda='none'):
	pos=getMinPos(y)
	x=changeStart(x,pos)
	y=changeStart(y,pos)
	
	sns.set_style("whitegrid")
	 
	# Color palette
	blue, = sns.color_palette("muted", 1)
	
	# Make the plot
	fig, ax = plt.subplots()
	ax.plot(x, y, color=blue, lw=3,label=legenda)
	ax.fill_between(x, 0, y, alpha=.3)
	ax.set(xlim=(0, len(x) - 1), ylim=(0, None), xticks=x)
	
	#Informações extra
	plt.title(titulo)
	plt.legend(loc='lower right', bbox_to_anchor=(1, -0.15))
	plt.savefig('output/'+titulo+'.png')

def makeBar(x,y,titulo = 'none',legenda='none'):
	plt.figure()
	# Create bars
	y_pos = np.arange(len(x))
	plt.bar(y_pos, y,label=legenda)
	
	# Create names on the x-axis
	plt.xticks(y_pos, x,fontname='Segoe UI Emoji', fontsize=13)
	
	#Informações extra
	plt.title(titulo)
	plt.legend(loc='lower right', bbox_to_anchor=(1, -0.15))
	
	# Create labels
	topLabel=[]
	for z in range(len(x)):
		topLabel.append(str(y[z]))

	# Text on the top of each barplot
	for i in range(len(topLabel)):
		plt.text(x = i, y = y[i], s = topLabel[i], size = 7)

	plt.grid(True, axis = 'y')
	plt.savefig('output/'+titulo+'.png')

def makeLine(x,y,titulo = 'none',legenda='none'):
	plt.figure(figsize=(20,10))
	# use the plot function
	printValue=[]
	steps = 1+int(len(x)/70)
	for z in range(len(x)):
		if(not z%steps): printValue.append(x[z]) 
	plt.plot(x,y,label=legenda)
	plt.xticks(ticks = printValue,rotation=90)
	plt.xlim(x[0],x[-1])
	plt.grid(True, axis = 'both')
	#Informações extra
	plt.title(titulo)
	plt.legend(loc='lower right', bbox_to_anchor=(1, -0.2))
	plt.savefig('output/'+titulo+'.png')

def makeConections(fromL,toL,title='none'):
	df = pd.DataFrame({ 'from':fromL, 'to':toL})
	# Build your graph
	plt.figure(figsize=(20,10))
	G = nx.from_pandas_edgelist(df, 'from', 'to')#,create_using=nx.DiGraph())
	# Graph with Custom nodes:
	#nx.random_layout(G)
	nx.draw(G, 
		with_labels=True,
		font_size=10, 
		font_color="red", 
		font_weight="bold",
		#pos=nx.circular_layout(G),
		#pos=nx.spectral_layout(G),
		#pos=nx.spring_layout(G),
		pos=nx.fruchterman_reingold_layout(G),
		node_shape = 's'
	)#, node_size=1500, node_color="skyblue", node_shape="s", alpha=0.5, linewidths=40)
	plt.savefig('output/'+title, dpi = 300)

def makeWordCloud(text,fileM=0,filename='none.pdf',stopW=['a']):
	plt.figure()
	if(fileM):
		#Cria uma mascara
		wave_mask = np.array(Image.open(fileM))
		# Create the wordcloud object
		wordcloud = WordCloud( mask=wave_mask,max_words=1000, stopwords=stopW, margin=0, background_color="white").generate(text)
	else:
		wordcloud = WordCloud(max_words=1000, width=1600, height=900,stopwords=stopW, margin=0, background_color="white").generate(text)

	# Display the generated image:
	plt.imshow(wordcloud, interpolation='bilinear')
	plt.axis("off")
	plt.savefig('output/'+filename, dpi = 300)