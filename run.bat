REM http://timmyreilly.azurewebsites.net/python-pip-virtualenv-installation-on-windows/
@echo off
cls
set Projeto=reportwhatsapp
set file=report.py



IF NOT EXIST "%venvs%\%Projeto%\" (
	mkvirtualenv %Projeto%
	setprojectdir
	if exist "requiriments.txt" (
		echo Instala Requerimentos
		workon %Projeto% && pip install -r requiriments.txt
	)
)
echo executando o script
workon %Projeto% && pip freeze > requiriments.txt
::&& %file%
