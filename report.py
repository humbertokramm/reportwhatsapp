﻿import codecs
import collections
import argparse
import time
from time import mktime
from datetime import datetime
import matplotlib.pyplot as plt
from openpyxl import load_workbook
import os
from pprint import pprint


path = "output"
if not os.path.exists(path):
	os.makedirs(path)


#Local Libraries
from words import stopWords,espWords,espWord,dayWeekName,numeros,letras,letras2,simbolos,removeChar,simpleName
from contacts import importGoogleContact,searcContatc,checkReference
from graphics import makeRadar,makeLine,makeBar,makeArea,makeConections,makeWordCloud
from chartRace import makeChartRace, makeCorrelation

# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-r", "--report", help = "nome do arquivo do relatorio")
ap.add_argument("-i", "--data_i", help = "data de inicio no formato dd/mm/aaaa")
ap.add_argument("-o", "--data_o", help = "data de fim no formato dd/mm/aaaa")
ap.add_argument("-p", "--data_p", help = "nome da pessoa")
ap.add_argument("-m", "--data_m", help = "mascara da nuvem de palavras")
args = vars(ap.parse_args())

#Lista de gráficos a serem gerados
flag_ChartRac=flag_Contacts=flag_Msg_dia=flag_Msg_sem=flag_Msg_hor=flag_Msg_emo=flag_wordCloud=flag_Connection=flag_Correlation = True

#flag_ChartRace = False
#flag_Contacts = False
#flag_Msg_dia = False
#flag_Msg_sem = False
#flag_Msg_hor = False
#flag_Msg_emo = False
#flag_wordCloud = False
#flag_Connection = False
#flag_Correlation = False

#localização dos caracteres
loc_1o_bar = 2
loc_2o_bar = 5
loc_2_dots = 13
loc_trace = 17



#Verifica se veio algum nome pelo argumento

#Hora Inicial
startTimeStamp = str(args["data_i"])
if(startTimeStamp == 'None'):startTimeStamp = 0
else: startTimeStamp = (mktime(datetime.strptime(startTimeStamp, "%d/%m/%Y").timetuple()))

#Hora Final
endTimeStamp = str(args["data_o"])
if(endTimeStamp == 'None'):endTimeStamp = 0
else: endTimeStamp = (mktime(datetime.strptime(endTimeStamp, "%d/%m/%Y").timetuple()))

#Hora Final
person = str(args["data_p"])
if(person == 'None'):person = 0

#mascara
fileMascara = str(args["data_m"])
if(fileMascara == 'None'):fileMascara = 0

#Nome do Arquivo
filename = str(args["report"])
if(filename == 'None'):
	print('Passe o nome do relatorio por argumento como o exemplo abaixo:')
	print('main.py -r \"Conversa do Whatsapp ... .txt\"')
	print('ou...')
	print('python main.py -r \"Conversa do Whatsapp ... .txt\"')
	exit()

#Carrega o Relatório
f = open(filename, encoding='utf-8')
print ("Name of the file: ", f.name)

#Abre o arquivo de relatório
if(person == 0): reportName = 'Report_'+filename[:len(filename)-4]
else: reportName = 'Report_'+filename[:len(filename)-4]+' '+person
filenameSheet = reportName +'.xlsx'
rankEvolution = reportName +'.csv'
wordCloudName = reportName+'.png'
GraphName = reportName+' graph.png'

wb = load_workbook('template/template.xlsx')
ws1 = wb["campos separados"]
ws2 = wb["Resultado"]
ws3 = wb["Dados"]
ws4 = wb["Status"]


teste=[]
lines = f.readlines()
atividade=[]
a = 0
realLines = 0
position = 0
contador = 0
for line in lines:
	temp = line
	temp=temp.replace('	',' ')
	contador += 1
	if(len(temp)>15):
		if(contador<10): #imprime as 10 primeiras linhas para inspeção
			print(temp[loc_1o_bar],temp[loc_2o_bar],temp[loc_2_dots],temp[loc_trace])
			print(temp)
		#if(temp[2]=='/' and temp[5]=='/' and temp[11]==':' and temp[15]=='-' and a > 0):
		if(temp[loc_1o_bar]=='/' and temp[loc_2o_bar]=='/' and temp[loc_2_dots]==':' and temp[loc_trace]=='-' and a > 0):
			if(temp.find(':',loc_trace)<0):
				for x in range(len(espWords)):
					position = temp.find(espWords[x])
					if(position>loc_trace):
						temp1=[]
						temp2=[]
						temp1 = temp[position+1:]
						temp2 = temp[:position]
						temp = temp2+'	'+temp1
						atividade.append(realLines)
					if(line != temp):
						break
			elif(temp.find(espWord,loc_trace)>0):
				position = temp.find(espWord,loc_trace)
				temp1=[]
				temp2=[]
				temp1 = temp[position+1:]
				temp2 = temp[:position]
				temp = temp2+': '+temp1
				atividade.append(realLines)
			if(temp.find(': ')):
				temp=temp.replace(': ','	',1)
			temp = temp.replace(' ','	',1)
			temp = temp.replace(' - ','	',1)
			temp = temp.replace('\u200e','')
			temp = temp.replace('\xa0',' ')
			teste.append(temp)
			realLines+=1
		elif(realLines > 0):
			teste[realLines-1]=teste[realLines-1].replace('\n',' '+temp,1)
		a+=1
	elif(realLines > 0):
		teste[realLines-1]=teste[realLines-1].replace('\n',' '+temp,1)

#Monta um relatório de atividades do grupo
'''f.close()
f2 = open('handled_'+filename, 'w',encoding='utf-8')
f2.writelines(teste)
f2.close()'''

#Monta um relatório de atividades do grupo
'''atividades =[]
for x in range(len(atividade)):
	atividades.append(teste[atividade[x]])
f.close()
f2 = open('atividade_'+filename, 'w',encoding='utf-8')
f2.writelines(atividades)
f2.close()'''

#print('atividade ',atividade)

data = []
timeStamp = []
hora = []
nome = []
msg = []
weekDay = []
justHour = []

for x in range(len(teste)):
	tempList = teste[x].split('	')
	if(len(tempList)!=4):
		print('\nexiste um problema de interpretação no dia:')
		print('>>> ',tempList,'\n')
		print('provavelmente alguma palavra especial deverá ser adicionada no atributo [espWords]')
		exit()
	a,b,c,d = tempList
	tempData = a
	#tempData = a[:6]+'20'+a[6:] #usar somente neste foemato de data "09/04/19 22:26"
	tempTimeStamp = (mktime(datetime.strptime(tempData+b, "%d/%m/%Y%H:%M").timetuple()))
	if(tempTimeStamp>=startTimeStamp):
		if(tempTimeStamp<=endTimeStamp or endTimeStamp == 0):
			if(person==0 or person == c):
				data.append(tempData)
				hora.append(b)
				nome.append(c)
				msg.append(d)
				timeStamp.append(tempTimeStamp)
				weekDay.append(dayWeekName[datetime.fromtimestamp(tempTimeStamp).weekday()])
				justHour.append(b[:2])

if(person == 0): makeChartRace(data,nome,rankEvolution)

# Build a dataframe with your connections
listConn=[[],[]]
listNames = ['']
if(flag_Contacts): listNames = importGoogleContact()

#Palavras mais usadas
palavras = []
nMsg = len(msg)
nChar = 0
for x in range(len(msg)):
	if(msg[x] != '<Arquivo de mídia oculto>\n'):
		nChar += len(msg[x])
		newstr = removeChar(simbolos,msg[x])
		newstr = newstr.lower()
		newstr = newstr.split(" ")
		for y in range(len(newstr)):
			if(len(newstr[y])>2):
				#Monta a lista de correlação
				if(newstr[y][0]=='@'):
					if(checkReference(newstr[y])):
						listConn[0].append(nome[x])
						listConn[1].append(searcContatc(listNames,newstr[y]))
					#Substitui nomes da agenda
					for k in range(len(listNames)):
						newstr[y] = simpleName(searcContatc(listNames,newstr[y]))
				#Monta mapa com palavras separadas
				palavras.append(newstr[y])

#Labels
if(person == 0):person = 'Todos'
tituloG=[
	'Frequência Semanal - '+person+' - '+str(nMsg)+' Mensagens - '+str(nChar)+' letras',
	'Horário mais frequente - '+person+' - '+str(nMsg)+' Mensagens - '+str(nChar)+' letras',
	'Mensagens por dia - '+person+' - '+str(nMsg)+' Mensagens - '+str(nChar)+' letras',
	'Top Emojis - '+person+' - '+str(nMsg)+' Mensagens - '+str(nChar)+' letras',
	data[0]+' até '+data[-1]
]
ws2['A1']= tituloG[0]
ws2['A2']= tituloG[1]
ws2['A3']= tituloG[2]
ws2['A4']= tituloG[3]
ws2['A5']= tituloG[4]

#Armazena mensagens na planilha
for row in range(len(data)):
	ws1['A'+str(row+2)] = datetime.strptime(data[row]+hora[row], "%d/%m/%Y%H:%M")
	ws1['B'+str(row+2)]= nome[row]
	ws1['C'+str(row+2)]= msg[row]

#Monta a aba com as principais alterações do grupo
row = 0
for x in range(len(data)):
	if(msg[x].find('mudou o nome de ')>=0):
		row += 1
		ws4['A'+str(row+2)] = datetime.strptime(data[x]+hora[x], "%d/%m/%Y%H:%M")
		ws4['B'+str(row+2)]= nome[x]
		ws4['C'+str(row+2)]= msg[x]

#Monta arquivo com o rank
Rank=collections.Counter(nome)
RankNomes = Rank.most_common(257)
gth = 1
gth +=1
print(gth)

pprint(Rank)
#ranklist=[]
#rankkeys = list(Rank.keys())
#rankValues = list(Rank.values())
for row in range(len(RankNomes)):
	ws3['A'+str(row+2)]= RankNomes[row][0]
	ws3['B'+str(row+2)]= RankNomes[row][1]

try:
	print('Monta arquivo com o rank de mensagens por dia:')
	if(flag_Msg_dia):
		RankDate=collections.Counter(data)
		y = list(RankDate.values())
		t = list(RankDate.keys())
		total = len(t)
		for row in range(len(t)):
			print('\t', row, " de ",total, end='\r')
			tempTimeStamp = datetime.strptime(t[row], "%d/%m/%Y").date()
			ws3['D'+str(row+2)]= tempTimeStamp
			ws3['E'+str(row+2)]= y[row]
		makeLine(x=t,y=y,titulo=tituloG[2],legenda=tituloG[4])
except:
	print('\nErro')


try:
	if(flag_Msg_sem):
		print('Monta arquivo com o rank dos dias da semana:')
		RankWeek=collections.Counter(weekDay)
		y = list(RankWeek.values())
		t = list(RankWeek.keys())
		total = len(t)
		for row in range(total):
			print('\t', row, " de ",total, end='\r')
			ws3['G'+str(row+2)]= t[row]
			ws3['H'+str(row+2)]= y[row]
		makeRadar(listName=t,listKeys=y,titulo=tituloG[0],legenda=tituloG[4])
except:
	print('\nErro')


try:
	if(flag_Msg_hor):
		print('Monta arquivo com o rank da hora:')
		justHour.sort() #Coloca em ordem
		RankHour=collections.Counter(justHour)
		y = list(RankHour.values())
		t = list(RankHour.keys())
		total = len(t)
		for row in range(total):
			print('\t', row, " de ",total, end='\r')
			ws3['J'+str(row+2)]= int(t[row])
			ws3['K'+str(row+2)]= y[row]
		makeArea(t,y,titulo=tituloG[1],legenda=tituloG[4])
except:
	print('\nErro')


try:
	print('Monta arquivo com o rank de palavras:')
	RankPalavras=collections.Counter(palavras)
	listWords = RankPalavras.most_common(1000)
	row = 2
	total = len(listWords)
	for y in range(total):
		addWord = True
		print('\t', y, " de ",total, end='\r')
		for x in range(len(stopWords)):
			if(listWords[y][0] == stopWords[x]): addWord = False
		if(addWord):
			ws3['M'+str(row)]= listWords[y][0]
			ws3['N'+str(row)]= listWords[y][1]
			row += 1
except:
	print('\nErro')


try:
	if( (flag_wordCloud)or (flag_Msg_emo) ):
		print('Monta um texto em uma unica linha:')
		texto = " ".join(palavras)

except:
	print('\nErro')


try:
	if(flag_Msg_emo):
		print('Deixa apenas os emojis:')
		newstr = removeChar(numeros,texto)
		newstr = removeChar(letras,newstr)
		newstr = removeChar(letras2,newstr)
		newstr = removeChar([' ','@','\u200d','\u200b'],newstr)
		emojis=[]
		total = len(newstr)
		for x in range(len(newstr)):
			print('\t', x, " de ",total, end='\r')
			emojis.append(newstr[x])

		RankEmojis=collections.Counter(emojis)
		RankEmojis = RankEmojis.most_common(1000)
		RankEmojisN=[]
		RankEmojisK=[]
		
		print('Monta tabela dos emojis:')
		total = len(newstr)
		for row in range(len(RankEmojis)):
			print('\t', row, " de ",total, end='\r')
			ws3['P'+str(row+2)]= RankEmojis[row][0]
			ws3['Q'+str(row+2)]= RankEmojis[row][1]
			if(row < 20):
				RankEmojisN.append(RankEmojis[row][0])
				RankEmojisK.append(RankEmojis[row][1])
		makeBar(RankEmojisN,RankEmojisK,titulo=tituloG[3],legenda=tituloG[4])
except:
	print('\nErro')


try:
	print('Fecha o Arquivo de Excel:')
	wb.save('output/'+filenameSheet)
except:
	print('\nErro')


try:
	print('Monta a nuvem de Palavras')
	if(flag_wordCloud): makeWordCloud(text=texto,fileM=fileMascara,filename=wordCloudName,stopW=stopWords)
except:
	print('\nErro')


try:
	print('Build a dataframe with your connections')
	if(flag_Connection): makeConections(fromL=listConn[0],toL=listConn[1],title=GraphName)
except:
	print('\nErro')


try:
	print('Build a dataframe with your flag_Correlation')
	if(flag_Correlation): makeCorrelation(listConn,rankEvolution)
except:
	print('\nErro')

plt.show()

