# ReportWhatsapp

Tratando o relatório de mensagens do whatsapp

# Bibliotecas
verifique o arquivo `requirimentes.txt` para saber os pacotes utilizados.

# Preparação
baixe o relatório de mensagens do grupo de whatsapp através do aplicativo pelo opção exportar conversa do menu do grupo.

* [tutorial do whatsapp] de como exportar...

exporte seus contatos para um arquivo `contacts.csv` no mesmo formato do arquivo do google contatos.
* [tutorial de exportar contato] do google...

# Usando
chame no terminal o arquivo `report.py` e passe o arquivo do relatório do Whastapp com o argumento `-r`. Exemplo:

```sh
python report.py -r "Conversas do grupo X.txt"
```

## Relatório Personalisado
Use os argumentos abaixo para setar determinadas opções:
* `-i` data de inicio no formato **dd/mm/aaaa**
* `-o` data de fim no formato **dd/mm/aaaa**
* `-p` nome da pessoa (nome igual ao salvo nos contatos ou o número de telefone precedido de **@**)
* `-m` máscara da nuvem de palavras (ex: ```mascara.png```)

## Resultado
Alguns gráficos serão exibidos na tela e será gerado um arquvivo `*.xlsx` com o relatório.


[//]: # ()


   [tutorial do whatsapp]: <https://www.techtudo.com.br/dicas-e-tutoriais/noticia/2016/03/como-exportar-conversas-do-whatsapp-para-o-google-drive-no-iphone.html>
   [tutorial de exportar contato]: <https://support.google.com/contacts/answer/7199294?hl=pt-BR&ref_topic=6154204>