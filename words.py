stopWords = [
	'que','tem','mas','pra','com','uma','uns','umas','meu',
	'essa','esse','essas','esses','dos','das','pro','vez','nos','nas','mídia','oculta'
]


espWords=[' saiu',' entrou',' alterou',' mudou',' adicionou',' removeu',' criou',' agora',' alterado','cancelou','foi adicionado','não é mais um administrador','apagou','redefiniu o link','não é mais','fixou uma','Campes atualizou a duração das','Bohnenberger desativou']
espWord = ' mudou' #Usado quando alteram o nome do grupo
#PS> Eventualmente o whatsapp muda essa string salava em "espWord"
dayWeekName = ['segunda','terça','quarta','quinta','sexta','sabado','domingo']

letras = [
	'A','B','C','Ç','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z',
	'a','b','c','ç','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z',
]
numeros = [
	'0','1','2','3','4','5','6','7','8','9'
]

letras2 = [
	'À','Ã','Â','Á','Ä',
	'È','Ê','É','Ë',
	'Ì','Í','Î','Ï',
	'Ò','Ó','Ô','Õ','Ö',
	'Ù','Ú','Û','Ü',
	'à','ã','â','á','ä',
	'è','ê','é','ë',
	'ì','í','î','ï',
	'ò','ó','ô','õ','ö',
	'ù','ú','û','ü',
]

simbolos = [
	'\n','!','?','_','"','“','”','#','$','%','&','\'','(',')','*','+',',','-','.','/',':',';','<','=','>','	','[','\\',']','^','`','{','|','}',','
]

def removeChar (list,string):
	newstr = string
	for x in range(len(list)):
		newstr = newstr.replace(list[x],"")
	return newstr

def simpleName (name):
	name = name.lower()
	name = name.split(' ')
	return name[0]
