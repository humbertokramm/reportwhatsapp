import csv
from words import removeChar,letras,letras2,simbolos

def checkNumber(n):
	n1=n2='0'
	if(len(n)>15):
		position = n.find(':')
		n1 = n[:position].replace(',','').replace('R','')
		n = n[position+3:]
		position = n.find(':')
		n2 = n[:position].replace(',','').replace('R','')
		n1 = str(int(n1))
		n2 = str(int(n2))
	else:	n1 = str(int(n))
	
	if(len(n1) > 12):
		n1 = n1[:4]+n1[5:]
	if(len(n2) > 12):
		n2 = n2[:4]+n2[5:]
	return n1,n2

def importGoogleContact(name='contacts.csv',key=26):
	list1=[[],[]]
	list2=[[],[]]
	with open(name, 'rt',encoding="utf8") as f:
		reader = csv.reader(f)
		for row in reader:
			list1[0].append(row[0])
			list1[1].append(removeChar(['+',' ','-','(',')','\u200e','\u202a','\u202c'],row[key]))
	for x in range(len(list1[0])):
		if(len(list1[1][x])> 12):
			list2[0].append(list1[0][x])
			n1,n2=checkNumber(list1[1][x])
			list2[1].append('@'+n1)
			if(n2!='0'):
				list2[0].append(list1[0][x])
				list2[1].append('@'+n2)
	return list2
	
def searcContatc(names,number):
	for x in range(len(names[0])):
		if(number == names[1][x]): 
			number = names[0][x]
			break;
	return number

def checkReference(word):
	word = removeChar(letras,word)
	word = removeChar(letras2,word)
	word = removeChar(simbolos,word)
	word = removeChar(['@'],word)
	if(len(word)>10): return True
	else: return False

